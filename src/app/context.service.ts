import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ContextService {
  private tagManager: any;

  constructor() { }

  initTagManager() {
    this.tagManager = cordova.require('com.jareddickson.cordova.tag-manager.TagManager');
    this.tagManager.init(
      success => {
        alert('success init');
      },
      error => {
        alert('error init');
      },
      'GTM-WRH6C7P',
      1
    );
  }

  getTagManager() {
    return this.tagManager;
  }

}
