import { Component } from '@angular/core';
import { ContextService } from '../context.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  constructor(private contextService: ContextService) {}

  ionViewDidEnter() {
    this.contextService.getTagManager().pushEvent(
      () => {
        alert('success screen view');
      },
      () => {
        alert('error screen view');
      },
      {
        event: 'screenView',
        screenName: 'Test Ionic',
        uiLanguageCD: 'EN',
        userIDCD: ''
      }
    );
  }

  onPushScreenView() {
    this.contextService.getTagManager().pushEvent(
      () => {
        alert('success screen view');
      },
      () => {
        alert('error screen view');
      },
      {
        event: 'screenView',
        screenName: 'Test Ionic',
        uiLanguageCD: 'EN',
        userIDCD: ''
      }
    );
  }


  onPushData() {
    this.contextService.getTagManager().pushEvent(
      () => {
        alert('success track');
      },
      () => {
        alert('error track');
      },
      {
        event: 'eventTracker',
        eventCat: 'App Navigation',
        eventAct: 'Bottom',
        eventLbl: 'Ahmed',
        eventVal: 0,
        screenName: 'AhmedScreen',
        uiLanguageCD: 'EN',
        userIDCD: ''
      }
    );
  }

  onDispatchEvents() {
    this.contextService.getTagManager().dispatch(
      successDispatch => {
        alert('success dispatch');
      },
      errorDispatch => {
        alert('error dispatch');
      }
    );
  }

}
